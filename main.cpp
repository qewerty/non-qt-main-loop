#include <cstring>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <QSharedPointer>
#include <QCoreApplication>
#include <QTimer>

using namespace std;

namespace {

bool done = false;

} // namespace

class QtComponent
{
public:
    QtComponent()
    {
        initQApp();
        initTimer();
    }

    ~QtComponent()
    {
        // Deleted before _argv because _argv must be valid for the entire lifetime of the QCoreApplication object.
        _app.reset();
        delete[] _argv[0];
        delete[] _argv;
    }

    void initQApp()
    {
        _argc = 1;
        _argv = new char*[_argc];
        _argv[0] = new char[1];
        _argv[0][0] = '\0';
        _app.reset(new QCoreApplication(_argc, _argv));
        QObject::connect(_app.data(), &QCoreApplication::aboutToQuit, [] {
            std::cout << "QCoreApplication::aboutToQuit" << std::endl;
            done = true;
        });
    }

    void initTimer()
    {
        _timer.reset(new QTimer());
        QObject::connect(_timer.data(), &QTimer::timeout, [this] {
            std::cout << (_tack ? "tack" : "tick") << std::endl;
            _tack = !_tack;
        });
        _timer->start(1000);

        auto quitTimer = new QTimer(_timer.data());
        QObject::connect(quitTimer, &QTimer::timeout, qApp, [] {
            std::cout << "quitTimer::timeout" << std::endl;
            qApp->quit(); // Doesn't work without qt event loop.
        });
        quitTimer->start(5000);
    }

    void update()
    {
        _app->processEvents();
    }

    void exec()
    {
        _app->exec();
    }

private:
    int _argc;
    char** _argv;
    QSharedPointer<QCoreApplication> _app;
    QSharedPointer<QTimer> _timer;
    bool _tack{};
};

void withoutThread()
{
    QtComponent qtComponent;
    while(!done) // Some kind of a non-qt main loop.
    {
        qtComponent.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void withThread()
{
    std::thread thread([] {
        withoutThread();
    });
    if(thread.joinable())
        thread.join();
}

void withThread2()
{
    std::thread thread([] {
        QtComponent qtComponent;
        qtComponent.exec();
    });
    if(thread.joinable())
        thread.join();
}

int main()
{
    switch(2)
    {
        case 0:
            withoutThread();
        break;
        case 1:
            withThread();
        break;
        case 2:
            withThread2();
        break;
    }
    return 0;
}
